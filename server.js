
const express = require('express')


require('dotenv').config()


const helmet = require('helmet') 
const bodyParser = require('body-parser') 
const cors = require('cors')  
const morgan = require('morgan') 

var db = require('knex')({
  client: 'pg',
  connection: {
    host : '192.168.99.100',
    user : 'project1',
    password : 'terserah',
    database : 'project1'
  }
});


const main = require('./controllers/main')


const app = express()


const whitelist = ['http://localhost:4000']
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}
app.use(helmet())
app.use('*',cors())
app.use(bodyParser.json())
app.use(morgan('combined')) 


app.get('/', (req, res) => res.send('hello world'))
app.get('/users', (req, res) => main.getTableData(req, res, db))
app.post('/users', (req, res) => main.postTableData(req, res, db))
app.put('/users', (req, res) => main.putTableData(req, res, db))
app.delete('/users', (req, res) => main.deleteTableData(req, res, db))


app.listen(process.env.PORT || 4000, () => {
  console.log(`app is running on port ${process.env.PORT || 4000}`)
})
